﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Gamma.Translation;
using System;

public class UGUITranslator : MonoBehaviour, ITranslatable, IDisposable {

	public Text label;
	public string languageKey;
	
	void Awake()
	{
		Init();
	}
	
	void OnDestroy(){
		Dispose();
	}
	
	public void Init(){
		LanguageManager.instance.RegisterTranslatable(this);
		label.text = LanguageManager.instance.GetString(languageKey);
	}
	
	public void Dispose(){
		LanguageManager.instance.UnRegisterTranslatable(this);
	}

	#region ITranslatable implementation

	public void OnLanguageChanged (SystemLanguage language, LanguageManager languageManager)
	{
		label.text = languageManager.GetString(languageKey);
	}

	#endregion
}
