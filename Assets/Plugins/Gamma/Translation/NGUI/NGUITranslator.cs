#if NGUI
using UnityEngine;
using System.Collections;


public class NGUITranslator : MonoBehaviour {

	public UILabel label;
	public string languageKey;
	
	void Awake(){
		Init();
		label.text = LanguageManager.current.GetString(languageKey);
	}
	
	void OnDestroy(){
		Dispose();
	}
	
	private void Init(){
		LanguageManager.OnLanguageChanged += OnLanguageChanged;
	}
	
	private void Dispose(){
		LanguageManager.OnLanguageChanged -= OnLanguageChanged;
	}
	
	private void OnLanguageChanged(){
		label.text = LanguageManager.current.GetString(languageKey);
	}
}
#endif
