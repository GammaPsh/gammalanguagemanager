using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using JsonFx.Json;

namespace Gamma.Translation{
	public class LanguageManager {
		
		private SystemLanguage defaultLanguage = SystemLanguage.English;
		private bool detectLanguage = true;
		private string relativePath = "LanguageFiles/";
		private string languageFileName = "translation";
		

		private string FilePath;
		private Dictionary<string,string> activeLanguage;

		private SystemLanguage _usedLanguage;
		public SystemLanguage usedLanguage{
			get{
				return _usedLanguage;	
			}
		}
		
		private static LanguageManager _instance;
		public static LanguageManager instance {
			get{
				if(_instance == null){
					_instance = new LanguageManager();
				}
				return _instance;
			}
		}
		
		// Use this for initialization
		private LanguageManager() {
			
			if(detectLanguage)
				_usedLanguage = Application.systemLanguage;
			else
				_usedLanguage = SystemLanguage.Unknown;
			
			LoadLanguage();
			
			Resources.UnloadUnusedAssets();
		}

		private void LoadLanguage(){
			activeLanguage = JsonReader.Deserialize<Dictionary<string, string>>(GetLanguageString());
		}
		
		private string GetLanguageString() {
			UnityEngine.Object resource = Resources.Load(relativePath + languageFileName + "_" + _usedLanguage.ToString());
			if(resource != null && resource.GetType() == typeof(TextAsset)){
				return (resource as TextAsset).text;
			}	
			
			_usedLanguage = defaultLanguage;
			resource = Resources.Load(relativePath + languageFileName + "_" + _usedLanguage.ToString());
			if(resource != null && resource.GetType() == typeof(TextAsset)){
				return (resource as TextAsset).text;
			}

			Debug.LogError("Translation Could not be loaded, check if default is set up correctly.\nloadingPath: " + relativePath + languageFileName + "_" + _usedLanguage.ToString());
			return "";
		}

		public void SetLanguage(string Language){
			foreach(SystemLanguage lang in Enum.GetValues(typeof(SystemLanguage))){
				if(lang.ToString() == Language){
					SetLanguage(lang);
					return;
				}
			}
		}

		public void SetLanguage(SystemLanguage Language){
			_usedLanguage = Language;

			LoadLanguage();

			InvokeTranslatableListeners();

			Resources.UnloadUnusedAssets();
		}
		
		public string GetString(string key) {
			if(activeLanguage!= null && activeLanguage.ContainsKey(key)){
				return System.Convert.ToString(activeLanguage[key]);
			}else{
				//Debug.LogWarning("Key does not exist");
				return key;	
			}
		}
		
		public SystemLanguage GetUsedLanguage() {
			return _usedLanguage;
		}

		private List<ITranslatable> translatables;
		public void RegisterTranslatable(ITranslatable translatable)
		{
			if(translatables == null){
				translatables = new List<ITranslatable>();
			}

			if(translatables.Contains(translatable)){
				return;
			}

			translatables.Add(translatable);
		}

		public void UnRegisterTranslatable(ITranslatable translatable)
		{
			if(translatables == null){
				translatables = new List<ITranslatable>();
			}

			if(translatables.Contains(translatable)){
				translatables.Remove(translatable);
			}
		}

		private void InvokeTranslatableListeners()
		{
			if(translatables == null)
				return;

			for(int i=translatables.Count-1; i>-1; i--){
				if(translatables[i] != null)
					translatables[i].OnLanguageChanged(usedLanguage, this);
			}
		}
	}
}
