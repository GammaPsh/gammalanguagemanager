﻿using UnityEngine;
using System.Collections;

namespace Gamma.Translation{
	public interface ITranslatable {

		void OnLanguageChanged(SystemLanguage language, LanguageManager languageManager);
	}
}
