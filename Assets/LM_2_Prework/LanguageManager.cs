/*using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using JsonFx.Json;
using Language;

namespace Language{
	// Language Codes in ISO 639-1 Code
	public enum LanguageShort {
		undefined,
		_aa, //Afar
		_ab, //Abkhazian
		_af, //Afrikaans
		_ak, //Akan
		_am, //Amharic
		_ar, //Arabic
		_an, //Aragonese
		_as, //Assamese
		_av, //Avaric
		_ae, //Avestan
		_ay, //Aymara
		_az, //Azerbaijani
		_ba, //Bashkir
		_bm, //Bambara
		_be, //Belarusian
		_bn, //Bengali
		_bh, //Bihari languages
		_bi, //Bislama
		_bo, //Tibetan
		_bs, //Bosnian
		_br, //Breton
		_bg, //Bulgarian
		_ca, //Catalan; Valencian
		_ch, //Chamorro
		_ce, //Chechen
		_cu, //Church Slavic; Old Slavonic
		_cv, //Chuvash
		_kw, //Cornish
		_co, //Corsican
		_cr, //Cree
		_cy, //Welsh
		_cs, //Czech
		_da, //Danish
		_de, //German
		_dv, //Divehi; Maldivian
		_dz, //Dzongkha
		_el, //Greek, Modern (1453)
		_en, //English
		_eo, //Esperanto
		_et, //Estonian
		_eu, //Basque
		_ee, //Ewe
		_fo, //Faroese
		_fa, //Persian
		_fj, //Fijian
		_fi, //Finnish
		_fr, //French
		_fy, //Western Frisian
		_ff, //Fulah
		_gd, //Gaelic; Scottish Gaelic
		_ga, //Irish
		_gl, //Galician
		_gv, //Manx
		_gn, //Guarani
		_gu, //Gujarati
		_ht, //Haitian
		_ha, //Hausa
		_he, //Hebrew
		_hz, //Herero
		_hi, //Hindi
		_ho, //Hiri Motu
		_hr, //Croatian
		_hu, //Hungarian
		_hy, //Armenian
		_ig, //Igbo
		_io, //Ido
		_ii, //Sichuan Yi
		_iu, //Inukitut
		_ie, //Interlingue; Occidental
		_ia, //Interlingua (International Auxiliary Language Association
		_id, //Indonesian
		_ik, //Inupiaq
		_is, //Icelandic
		_it, //Italian
		_jv, //Javanese
		_ja, //Japanese
		_kl, //Kalaallisut; Greenlandic
		_kn, //Kannada
		_ks, //Kashmiri
		_ka, //Georgian
		_kr, //Kanuri
		_kk, //Kazakh
		_km, //Central Khmer
		_ki, //Kikuyu; Gikuyu
		_rw, //Kinyarwanda
		_ky, //Kirghiz; Kyrgyz
		_kv, //Komi
		_kg, //Kongo
		_ko, //Korean
		_kj, //Kuanyama
		_ku, //Kurdish
		_lo, //Lao
		_la, //Latin
		_lv, //Latvian
		_li, //Limburgan
		_ln, //Lingala
		_lt, //Lithuanian
		_lb, //Luxembourgish
		_lu, //Luba-Katanga
		_lg, //Ganda
		_mk, //Macedonian
		_mh, //Marshallese
		_ml, //Malayalam
		_mi, //Maori
		_mr, //Marathi
		_mg, //Malagasy
		_mt, //Maltese
		_mn, //Mongolian
		_ms, //Malay
		_my, //Burmese
		_na, //Nauru
		_nv, //Navajo
		_nr, //Ndebele South
		_nd, //Ndebele North
		_ne, //Nepali
		_nl, //Dutch; Flemish
		_nn, //Norwegian
		_nb, //Bokmal Norwegian
		_ny, //Chichewa
		_oc, //Occitan (post 1500)
		_oj, //Ojibwa
		_or, //Oriya
		_om, //Oromo
		_os, //Ossetian
		_pa, //Panjabi
		_pi, //Pali
		_pl, //Polish
		_pt, //Portuguese
		_ps, //Pushto; Pashto
		_qu, //Quechua
		_rm, //romansh
		_ro, //Romanian
		_rn, //Rundi
		_ru, //Russian
		_sg, //Sango
		_sa, //Sanskrit
		_si, //Sinhala, Sinhalese
		_sk, //Slovak
		_sl, //Slovenian
		_se, //Northern Sami
		_sm, //Samoan
		_sn, //Shona
		_sd, //Sindhi
		_so, //Somali
		_st, //Sotho, Southern
		_es, //Spanish, Castallian
		_sq, //Albanian
		_sc, //Sardinian
		_sr, //Serbian
		_ss, //Swati
		_su, //Sudanese
		_sw, //Swahili
		_sv, //Swedish
		_ty, //Tahitian
		_ta, //Tamil
		_tt, //Tatar
		_te, //Telugu
		_tg, //Tajik
		_tl, //Tagalog
		_th, //Thai
		_ti, //Tigrinya
		_to, //Tonga
		_tn, //Tswana
		_ts, //Tsonga
		_tk, //Turkmen
		_tr, //Turkish
		_tw, //Twi
		_ug, //Uighur, Uyghur
		_uk, //Ukrainian
		_ur, //Urdu
		_uz, //Uzbek
		_ve, //Venda
		_vi, //Vietnamese
		_vo, //Volapük
		_wa, //Walloon
		_wo, //Wolof
		_xh, //Xhosa
		_yi, //Yiddish
		_yo, //Yoruba
		_za, //Zhuang; Chuang
		_zh, //Chinese
		_zu // Zulu
	}
	
	public class LanguageHelper{
		
		private Dictionary<SystemLanguage, LanguageShort> languageCodeTable;
		
		public LanguageHelper(){
			languageCodeTable = new Dictionary<SystemLanguage, LanguageShort>();
			
			languageCodeTable.Add(SystemLanguage.Afrikaans, LanguageShort._af);
			languageCodeTable.Add(SystemLanguage.Arabic, LanguageShort._ar);
			languageCodeTable.Add(SystemLanguage.Basque, LanguageShort._eu);
			languageCodeTable.Add(SystemLanguage.Belarusian, LanguageShort._be);
			languageCodeTable.Add(SystemLanguage.Bulgarian, LanguageShort._bg);
			languageCodeTable.Add(SystemLanguage.Catalan, LanguageShort._ca);
			languageCodeTable.Add(SystemLanguage.Chinese, LanguageShort._zh);
			languageCodeTable.Add(SystemLanguage.Czech, LanguageShort._cs);
			languageCodeTable.Add(SystemLanguage.Danish, LanguageShort._da);
			languageCodeTable.Add(SystemLanguage.Dutch, LanguageShort._nl);
			languageCodeTable.Add(SystemLanguage.English, LanguageShort._en);
			languageCodeTable.Add(SystemLanguage.Estonian, LanguageShort._et);
			languageCodeTable.Add(SystemLanguage.Faroese, LanguageShort._fo);
			languageCodeTable.Add(SystemLanguage.Finnish, LanguageShort._fi);
			languageCodeTable.Add(SystemLanguage.French, LanguageShort._fr);
			languageCodeTable.Add(SystemLanguage.German, LanguageShort._de);
			languageCodeTable.Add(SystemLanguage.Greek, LanguageShort._el);
			languageCodeTable.Add(SystemLanguage.Hebrew, LanguageShort._he);
			languageCodeTable.Add(SystemLanguage.Hungarian, LanguageShort._hu);
			languageCodeTable.Add(SystemLanguage.Icelandic, LanguageShort._is);
			languageCodeTable.Add(SystemLanguage.Indonesian, LanguageShort._id);
			languageCodeTable.Add(SystemLanguage.Italian, LanguageShort._it);
			languageCodeTable.Add(SystemLanguage.Japanese, LanguageShort._ja);
			languageCodeTable.Add(SystemLanguage.Korean, LanguageShort._ko);
			languageCodeTable.Add(SystemLanguage.Latvian, LanguageShort._lv);
			languageCodeTable.Add(SystemLanguage.Lithuanian, LanguageShort._lt);
			languageCodeTable.Add(SystemLanguage.Norwegian, LanguageShort._nn);
			languageCodeTable.Add(SystemLanguage.Polish, LanguageShort._pl);
			languageCodeTable.Add(SystemLanguage.Portuguese, LanguageShort._pt);
			languageCodeTable.Add(SystemLanguage.Romanian, LanguageShort._ro);
			languageCodeTable.Add(SystemLanguage.Russian, LanguageShort._ru);
			languageCodeTable.Add(SystemLanguage.SerboCroatian, LanguageShort._hr);
			languageCodeTable.Add(SystemLanguage.Slovak, LanguageShort._sk);
			languageCodeTable.Add(SystemLanguage.Slovenian, LanguageShort._sl);
			languageCodeTable.Add(SystemLanguage.Spanish, LanguageShort._es);
			languageCodeTable.Add(SystemLanguage.Swedish, LanguageShort._sv);
			languageCodeTable.Add(SystemLanguage.Thai, LanguageShort._th);
			languageCodeTable.Add(SystemLanguage.Ukrainian, LanguageShort._uk);
			languageCodeTable.Add(SystemLanguage.Vietnamese, LanguageShort._vi);
			languageCodeTable.Add(SystemLanguage.Unknown, LanguageShort.undefined);
		}
		
		public LanguageShort GetLanguageIsoCode(SystemLanguage englishName){
			return languageCodeTable[englishName];
		}
		
		public SystemLanguage GetLanguageEnglishName(LanguageShort isoCode){
			if(!languageCodeTable.ContainsValue(isoCode))
				return SystemLanguage.Unknown;
			
			foreach(KeyValuePair<SystemLanguage, LanguageShort> pair in languageCodeTable){
				if(pair.Value == isoCode){
					return pair.Key;	
				}
			}
			return SystemLanguage.Unknown;
		}
	}
}


public class LanguageManager : MonoBehaviour {
	
	public bool useSystemLanguage;
	public LanguageShort defaultLanguage;
	
	private static Dictionary<string,string> table;
	private static LanguageShort _defaultLanguage;
	private static LanguageShort _usedLanguage;
	
	public static SystemLanguage usedSystemLanguage{
		get{
			LanguageHelper helper = new LanguageHelper();
			return helper.GetLanguageEnglishName(_usedLanguage);	
		}
	}
	public static LanguageShort usedLangugageIso{
		get{
			return _usedLanguage;
		}
	}

	// Use this for initialization
	void Awake () {
		_defaultLanguage = defaultLanguage;
		
		if(useSystemLanguage)
			ParseLanguageFile(LoadLanguageFile(Application.systemLanguage));
		else
			ParseLanguageFile(LoadLanguageFile(_defaultLanguage));
	}
	
	private static string LoadLanguageFile(SystemLanguage desiredLanguage){
		LanguageHelper helper = new LanguageHelper();
		return LoadLanguageFile(helper.GetLanguageIsoCode(desiredLanguage));
	}
	
	private static string LoadLanguageFile(LanguageShort desiredLanguage){
		
		Debug.Log("Loading Rescource File: " + desiredLanguage.ToString());
		
		_usedLanguage = desiredLanguage;
		UnityEngine.Object loadedFile = Resources.Load("LanguageFiles/translation" + _usedLanguage.ToString());
		
		if(loadedFile == null){
			_usedLanguage = _defaultLanguage;
			loadedFile = Resources.Load("LanguageFiles/translation" + _defaultLanguage.ToString());
		}
		
		if(loadedFile == null){
			Debug.LogWarning("LanguageFile not Found, Please check Defaultlanguage and LanguageFiles");
			return "";
		}
		return (loadedFile as TextAsset).text;
	}
	
	private static void ParseLanguageFile(string languageString){
		table = JsonReader.Deserialize<Dictionary<string,string>>(languageString);
	}
	
	//Loading a new Language using a string to define the Language, English languagename or Iso 639-1 Format are Accepted
	public static void ChangeLanguage(string language){
		
		// Check if Input is an Iso 639-1 Language Format, if true find English name
		if(language.Length < 3 && Enum.IsDefined(typeof(LanguageShort), language)){
			ParseLanguageFile(LoadLanguageFile((LanguageShort)Enum.Parse(typeof(LanguageShort), language)));
			return;
		}
		
		if(language.Contains("_")){
			ChangeLanguage(language.Replace("_", ""));
			return;
		}
		
		// if Language is part of SystemLanguage Enum switch to desired Language, else load defaultLanguage
		if (Enum.IsDefined(typeof(SystemLanguage), language)){
      		ParseLanguageFile(LoadLanguageFile((SystemLanguage)Enum.Parse(typeof(SystemLanguage), language)));
		} else {
     		ParseLanguageFile(LoadLanguageFile(_defaultLanguage));
			Debug.LogWarning("Desired Language not found, take default: " + _defaultLanguage.ToString());
		}
	}
	
	//Loading a new Language using a SystemLange enum value
	public static void ChangeLanguage(SystemLanguage language){
		ParseLanguageFile(LoadLanguageFile(language));
	}
	
	//Loading a new Language using a LanguageShort enum value
	public static void ChangeLanguage(LanguageShort language){
		ParseLanguageFile(LoadLanguageFile(language));
	}
	
	//Gets a translated string value for the corresponding key
	public static string GetString(string key){
		
		if(!table.ContainsKey(key)){
			Debug.LogWarning("LanguageManager did not found key: " + key);
			return "";
		}
		return table[key];
	}
}
*/
